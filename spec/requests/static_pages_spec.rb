require 'spec_helper'

describe "Static pages" do

  subject { page }

  shared_examples_for "all static pages" do
    it { should have_selector('h1',    text: heading) }
    it { should have_selector('title', text: full_title(page_title)) }
  end

  describe "Home page" do
    before { visit root_path }

    let(:heading) { 'Welcome to Betsy' }
    let(:page_title) { '' }
    it { should have_selector('a', :text => 'Help') }

  end

  describe "Help page" do
    before { visit help_path }

    let(:page_title) { 'Help' }
    let(:heading) { "Help" }

  end

  describe "About page" do
    before { visit about_path }

    let(:page_title) { 'About' }
    let(:heading) { "About" }
    it { should have_content('Donald') }

  end

  describe "Contact page" do
    before { visit contact_path }

    let(:page_title) { 'Contact' }
    let(:heading) { "Contact" }
    it { should have_content("donald@betsy.com") }

  end

  it "should have the right links on the layout" do
    visit root_path
    click_link "About"
    page.should have_selector 'title', text: full_title('About')
    click_link "Help"
    page.should have_selector 'title', text: full_title('Help')
    click_link "Contact"
    page.should have_selector 'title', text: full_title('Contact')
    click_link "Betsy"
    page.should have_selector 'title', text: full_title('')
    click_link "Sign Up Now"
    page.should have_selector 'title', text: full_title('Sign Up')
  end

end
